package main

import (
	"fmt"
	"io/ioutil"
	"os/exec"

	yaml "gopkg.in/yaml.v2"
)

const (
	verifyCommandNotExisting = iota
	verifyArgumentVariable
)

// VerifyError describes an error
type verifyError struct {
	Status  int
	Message string
}

func (ve *verifyError) Error() string {
	return fmt.Sprintf("Verify Error %d: %s", ve.Status, ve.Message)
}

type endPoint struct {
	Path        string   `yaml:"path"`
	Command     string   `yaml:"command"`
	Arguments   []string `yaml:"arguments"`
	ContentType string   `yaml:"content_type"`
}

type configuration struct {
	ListenServer string     `yaml:"listen_host"`
	ListenPort   int        `yaml:"listen_port"`
	EndPoints    []endPoint `yaml:"end_points"`
}

func (c *configuration) getHostString() string {
	return fmt.Sprintf("%s:%d", c.ListenServer, c.ListenPort)
}

func readConfig(path string) (*configuration, error) {
	dat, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	var configFile configuration
	err = yaml.Unmarshal([]byte(dat), &configFile)
	if err != nil {
		return nil, err
	}
	return &configFile, nil
}

func verifyArgumentVariables(arguments []string) error {
	allowedVariables := getAllowedArgumetnVariables()
	for _, argument := range arguments {
		variables := getVariables(argument)
		for _, v := range variables {
			found := false
			for _, allowedVariable := range allowedVariables {
				if v == allowedVariable {
					found = true
				}
			}
			if !found {
				return &verifyError{verifyArgumentVariable, fmt.Sprintf("Unknown variable: %s", v)}
			}
		}
	}
	return nil
}

func verifyConfiguration(config *configuration) error {
	for _, endPoint := range config.EndPoints {
		_, err := exec.LookPath(endPoint.Command)
		if err != nil {
			return &verifyError{verifyCommandNotExisting, err.Error()}
		}
		err = verifyArgumentVariables(endPoint.Arguments)
		if err != nil {
			return err
		}
	}
	return nil
}
