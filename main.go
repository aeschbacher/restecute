package main

import (
	"net/http"
)

func main() {
	config, err := readConfig("main.conf")
	if err != nil {
		panic(err)
	}
	err = verifyConfiguration(config)
	if err != nil {
		panic(err)
	}
	r := generateHandlers(config)
	http.ListenAndServe(config.getHostString(), r)
}
