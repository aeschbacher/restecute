package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"

	"github.com/gorilla/mux"
)

func writeError(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte(fmt.Sprintf("500 - %s!", err.Error())))
}

func writeInFile(r *http.Request) (string, error) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return "", err
	}
	tmpFile, err := ioutil.TempFile(os.TempDir(), "prefix-")
	if err != nil {
		return "", err
	}
	defer tmpFile.Close()
	tmpFile.Write(data)
	return tmpFile.Name(), nil
}

func writeResponse(w http.ResponseWriter, contentType string, outData bytes.Buffer) {
	if contentType == "" {
		contentType = "text/plain; charset=utf-8"
	}
	w.Header().Add("Content-Type", contentType)
	w.Write(outData.Bytes())
}

func mkHandleFunc(endPoint endPoint) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		inFileName, err := writeInFile(r)
		defer os.Remove(inFileName)
		if err != nil {
			writeError(w, err)
			return
		}
		vars := map[string]string{}
		vars["infile"] = inFileName
		arguments, err := substituteVariables(endPoint.Arguments, vars)
		if err != nil {
			writeError(w, err)
			return
		}
		cmd := exec.Command(endPoint.Command, arguments...)
		var out bytes.Buffer
		cmd.Stdout = &out
		cmd.Run()

		writeResponse(w, endPoint.ContentType, out)
	})
}

func generateHandlers(config *configuration) *mux.Router {
	r := mux.NewRouter()
	for _, endPoint := range config.EndPoints {
		r.HandleFunc(endPoint.Path, mkHandleFunc(endPoint))
	}
	return r
}
