package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

type EndPointTest struct {
	EndPoint            endPoint
	Method              string
	URL                 string
	Data                []byte
	ExpectedBody        []byte
	ExpectedStatusCode  int
	ExpectedContentType string
}

func getMinimalTest() EndPointTest {
	return EndPointTest{
		endPoint{"/minimal", "echo", []string{"hello world"}, ""},
		"GET",
		"/minimal",
		[]byte{},
		[]byte("hello world\n"),
		200,
		"text/plain; charset=utf-8",
	}
}

func getContentTypeTest() EndPointTest {
	contentType := "application/json; charset=utf-8"
	contentTypeTest := getMinimalTest()
	contentTypeTest.EndPoint = endPoint{"/minimal", "echo", []string{"hello world"}, contentType}
	contentTypeTest.ExpectedContentType = contentType
	return contentTypeTest
}

func getInFileTest() EndPointTest {
	inFileTest := getMinimalTest()
	inFileTest.Data = []byte("This is the input.")
	inFileTest.EndPoint = endPoint{"/infile", "cat", []string{"${infile}"}, ""}
	inFileTest.ExpectedBody = inFileTest.Data
	return inFileTest
}

func executeHandler(td EndPointTest) error {
	body := bytes.NewReader(td.Data)
	r := httptest.NewRequest(td.Method, td.URL, body)
	w := httptest.NewRecorder()
	handlerFun := mkHandleFunc(td.EndPoint)
	handlerFun(w, r)
	return checkResponse(w.Result(), td)
}

func checkResponse(resp *http.Response, td EndPointTest) error {
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if string(td.ExpectedBody) != string(body) {
		return fmt.Errorf("response body not as expected: %v, %v", string(td.ExpectedBody), string(body))
	}
	if resp.StatusCode != td.ExpectedStatusCode {
		return fmt.Errorf("Status code, got: %d, wanted: %d", resp.StatusCode, td.ExpectedStatusCode)
	}
	contentType := resp.Header.Get("Content-Type")
	if contentType != td.ExpectedContentType {
		return fmt.Errorf("Content-Type, got %s, wanted: %s", contentType, td.ExpectedContentType)
	}
	return nil
}

func TestEndPoints(t *testing.T) {
	minimalTest := getMinimalTest()
	contentTypeTest := getContentTypeTest()
	inFileTest := getInFileTest()
	tests := []EndPointTest{minimalTest, contentTypeTest, inFileTest}
	for idx, endPointTest := range tests {
		if err := executeHandler(endPointTest); err != nil {
			t.Fatalf("Running Test %d: %v", idx, err)
		}
	}
}

func TestWriteInFile(t *testing.T) {
	bodyData := "Some Data."
	body := bytes.NewReader([]byte(bodyData))
	r := httptest.NewRequest("GET", "/url", body)
	fileName, err := writeInFile(r)
	readData, err := ioutil.ReadFile(fileName)
	if err != nil {
		t.Fatal(err)
	}
	if bodyData != string(readData) {
		t.Fatal("data not equal")
	}
}
